#!/usr/bin/perl
# This file takes a list of markdown files,
# images, and breakout texts, and attempts to create the master
# layout.tex file. 
# 
# Make a bulletin from various Markdown documents
# Copyright (C) 2005 Free Software Foundation, Inc
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
# See CONFIGURE below for more details on how to build the list
#
# It assumes: 
# * SmartyPants.pl is in the same directory
# * markdown is installed
# * gnuhtml2latex is installed
# * environment variable TEXFILES has the path to Bulletin/include
# 
# Notes:
# If you wish to insert an image in the middle of a file,
# or breakout text, it is recommended that you break the markdown file
# into parts and to insert 
#
##################################################
## CONFIGURE
#

my $ISSUE = 16;
my $MONTH = "May";
my $YEAR = "2010";

# @file(
# ["ARTICLE", "FILENAME", "TITLE", "AUTHOR-NAME", "AUTHOR-TITLE", page-number],
# ["IMAGE", "path", "caption"],
# ["BREAK", "text"] 
# );

my @contents =  
    (
["ARTICLE", "foo.mdwn", "This is an example", "by Richard Stallman", "President",1],
     );

## END CONFIGURE
##################################################

# Global
my $blockquote = 0;  # truned on when we are in a blockquote

# SUBROUTINES

# process latex files from gnuhtml2latex one line at a time
sub latex {
    my($str) = @_;

    $str =~ s/\\par //g;   #strip "\par ". 

    # sometimes funny characters are generated. We deal with them here.
    $str =~ s/“/\`\`/g;    #left quote
    $str =~ s/”/\'\'/g;    #right quote
    $str =~ s/‘/\`/g;      #tick
    $str =~ s/’/\'/g;      #right tick
    $str =~ s/—/\---/g;    #em-dash
    $str =~ s/—-/\---/g;   #em-dash dash. 
    $str =~ s/…/\\ldots/g; #ellipsis

    # FOOTNOTE:
    if($str =~ m/FOOTNOTE:/) {
	$str =~ s/FOOTNOTE:\\/\\footnote/g;
	$str =~ s/\\\}/\}/g;
    }

    # URL:\{bdb.org \} -> \url{dbd.org}
    if($str =~ m/URL:/i) {
	$str =~ s/URL:\\/\\url/g;
	$str =~ s/\\\}/\}/g;
    }

    # images in the form IMAGE:PATH:CAPTION:CENTER:TYPE
    # on their own line.
    # e.g., IMAGE:/home/jgay/image.eps:This is a swell image.:center:htb
    # where IMAGE is just a keyword, 
    # and htb is the figure style, e.g, \\begin{figure}[htb]
    if($str =~ m/image:/i) {
	my($img, $path, $caption, $style, $type) = split(/:/, $str);
	chomp($type);
	$str  = "\n\n";
	$str .= "\\begin{figure}[".$type."]\n";
	$str .=	($style =~ m/center/i) ? "  \\begin{center}\n": "";
	$str .=	"    \\includegraphics[width=2in]{".$path."}\n";
	$str .=	($style =~ m/center/i) ? "  \\end{center}\n": "";
	$str .= ($caption ne "") ? "\n\\caption{\\small{".$caption."}}\n":"";
	$str .= "\\end{figure}\n\n";
	return $str;
    }

    # Appropriately deal with blockquotes
    if($str =~ /^\s\s/) {
	if($blockquote == 0) { #start of blockquote
	    $str =~ s/^\s\s/\\begin{quotation}\n/g; 
	    $blockquote = 1; 
	} else {  #within blockquote
	    $str =~ s/^\s\s//g; #remove leading space
	}
    } elsif($blockquote > 0) {
	$str = "\\end{quotation}\n".$str; #end of blockquote
	$blockquote = 0;
    }	
	
    return $str;
}

# MAIN
print "\\documentclass[letterpaper]{article}\n"; 
print "\\usepackage{bulletin-new}\n"; 
print "\\begin{document}\n";
print "\\twocolumn[\\masthead{bulletin-logo.eps}{Bulletin}{Issue " . $ISSUE . "}{". $MONTH . " " . $YEAR . "}]\n"; 

print "% Table of contents\n";
print "\\setcounter{secnumdepth}{-1} % no numbering on sections in the TOC\n";
print "\\noindent\n";
print "\\colorbox{Myblue}{%\n";
print "  \\begin{tabular}{p{1.5in}p{0.12in}}\n";
print "    \\Large\\textbf{Contents} \\\\\n";
print "      \\\\\n";
for my $i (0..$#contents) {

    if($contents[$i][0] eq "ARTICLE") {
	print "      \\raggedright\\normalsize{{" . $contents[$i][2] . "}} & {" . $contents[$i][5] . "}\\\n";
	print "      \\\\\n";
    }
}
print "  \\end{tabular}\n";
print "}\n";
print "\n\n";

## BODY OF DOCUMENT
for my $i (0..$#contents) {
    #IF ARTICLE
    if($contents[$i][0] eq "ARTICLE") {
	# print section info
	print "% START: ".$contents[$i][2]."\n";
	print "\\section{" . $contents[$i][2] . "}\n";
        if($contents[$i][4]) {
	    print "\\textbf{\\textit{".$contents[$i][3]."\\\\".$contents[$i][4]."}}\n";
	} else {
	    print "\\textbf{\\textit{".$contents[$i][3]."}}\n";
	}

	#create html from markdown
	my @mdwn = `markdown $contents[$i][1]`;

	#format HTML so gnuhtml2tex can understand it
	my $in = "<html><head><title></title></head><body>" . join("", @mdwn) . "</body></html>";
	#write HTML out to temporary file
	$filename = $contents[$i][1];
	$filename =~ s/\.mdwn?$//; 
	$outfile  = ".mdwn2tex_".$filename.".html"; # .mdwn2tex_FILE.html
	open(IN, ">$outfile");
	print IN $in;
	close(IN);

	
	# Run SmartyPants on the output to fix quotes and em-dashes
	my @smarty = `perl SmartyPants.pl $outfile`;
	$outfile2 = "smarty".$outfile;
	open(IN, ">$outfile2");
	print IN join("",@smarty);
	close(IN);


	# run gnuhtml2latex on temporary file
	my @tex = `gnuhtml2latex -s $outfile2 -h ""`;
	`rm $outfile`;                     
	`rm $outfile2`;                    
	@tex = splice(@tex, 6, $#tex - 6); # strip off header and footer info

	my $first_par = 0;

	foreach my $line (@tex) {

	    if($first_par > 0) {
		print &latex($line);
	    } 
	    else { 
		if($line =~ /^\\par/) { 
		    $line = &latex($line);
		    $line =~ s/\w/\\cap{$&}/; # Capitalize first letter
		    print $line;
		    $first_par = 1;
		} else {
		    print &latex($line);
		}
	    }
	}	    
	
	print "\\ender\n% END: ".$contents[$i][2]."\n\n\n"; #\ender
    }

    #IF IMAGE
    if($contents[$i][0] eq "IMAGE") {
	# print section info
	print "\n";
	print "\\begin{figure}[htb]\n";
	print "  \\begin{center}\n";
	print "    \\includegraphics[width=2in]{".$contents[$i][1]."}\n";
	print "  \\end{center}\n";
	print "  \\caption{\\small{".$contents[$i][2]."}}\n";
	print "\\end{figure}\n";	
	print "\n";
    }

} # end main loop

# footer info
print "\\verbatimcopyright{$YEAR}\n";
print "\\donate{foo}\n";
print "\\end{document}\n";

