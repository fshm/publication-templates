\restoregeometry

\begin{center}

        \vspace*{0.2cm}


       {\LARGE\mytitle{மூன்றாம் கண்}}

       \vspace{0.5cm}

       {\LARGE\chapterheading{ஏற்காடு இளங்கோ}}

        \vspace{0.5cm}


        \vspace{1.5cm}


        \LARGE\textbf{மின்னூல் வெளியீடு : freetamilebooks.com}

        \vspace{1.0cm}

        \Large\mani{உரிமை-Creative Commons Attribution-NonCommercial-NoDerivatives 4.0}

        \vspace{1.0cm}

        \Large\mani{உரிமை - கிரியேட்டிவ் காமன்ஸ். எல்லாரும் படிக்கலாம், பகிரலாம்.}

        \vspace{1.0cm}
  
        \Large\mani{ மின்னூலாக்கம் - லெனின் குருசாமி - guruleninn@gmail.com}

        \Large\mani{ அட்டைப்படம் - மனோஜ் குமார் - socrates1857@gmail.com}

        \vfill

        \vspace{3.0cm}

        \Large\mani{ This Book was produced using LaTeX + Pandoc}


     \end{center}
    \thispagestyle{empty}
\newpage
\thispagestyle{empty}
\mbox{}
