\restoregeometry

\begin{center}

        \vspace*{0.2cm}


       {\LARGE\mytitle{மூன்றாம் கண்}}

       \vspace{0.5cm}

       {\LARGE\chapterheading{ஏற்காடு இளங்கோ}}

        \vspace{0.5cm}


        \vspace{3.5cm}


        \Large\textbf{மின்னூல் வெளியீடு : freetamilebooks.com}

        \vspace{1.0cm}

        \newpage

        \large\mani{உரிமை-Creative Commons Attribution-NonCommercial-NoDerivatives 4.0}

        \vspace{1.0cm}

        \large\mani{உரிமை - கிரியேட்டிவ் காமன்ஸ். எல்லாரும் படிக்கலாம், பகிரலாம்.}

        \vspace{1.0cm}
  
        \large\mani{ மின்னூலாக்கம் - லெனின் குருசாமி - guruleninn@gmail.com}

        \large\mani{ அட்டைப்படம் - மனோஜ் குமார் - socrates1857@gmail.com}

        \vfill

        \vspace{1.0cm}

        \large\mani{ This Books was produced using LaTeX + Pandoc}

        \vfill

     \end{center}
    \thispagestyle{empty}
\newpage
\thispagestyle{empty}
\mbox{}
